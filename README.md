# usingPacker

This is a development repo to learn and start creating custom images using Hashicorp's packer.

## Packer Provisioning

Packer will create a temporary ec2 instance to get create the ami. This will also create an temp vpc, security groups as well. Once the instance is created, then it will create a snapshot from that instance, then it automatically distroy the instance along with the other resources it might have created. 

If we are using the same name, you will get an error saying that there is an existing image with the same name. Once the image is create, the snapshots are still present. we need to make sure that snapshot are deleted as they take up a lot of stuff. 

Packer runs sequentially, if there are more than one provisioner blocks, the packer runs them one after another. 

### Steps:

1. Create a base image to build upon. 
2. using provisioner to build upon the base image. 
    a. this is done by using the ssh username to ssh into the image to install all the provisioning that needes to be done. 
3. Everytime you are building a new image on top of the image, you need a new image name as the image names are supposed to be unique. 
4. We can also also run entire shell scripts instead of the provisioner blocks. (need to test this out). 
5. In order to install plugin or validate the packer file you always need to intialize packer. 

## Variables

There are many ways we can define varibles in packer
    1. inline within the code
    2. variables file 
    3. command line flag

Order of precidence is -> command line flag will override any of the other varible declarations followed by the varible file and inline. 

When using auto.varible it is imperative that we are using packer build . to build the image if you use packer build < imagefile.pkrhcl > then the auto variable file will not run. The varibles must be declared within the main code to utilize them. 

Locals are local varibles that can be declared by using the key word local. 

## Parallel Builds

Creating multiple images based on the same template across platforms. you can have multiple sources and include it in the build block. All the sources included in the build block are created in parallel. Based on source blocks you can create images in cloud, docker etc all in a single file. 

## Post Processors

There processors are executed only after the ami has been build and these processors can vary in functions. They can push the image to cloud if you want. You can add as many post processors can be added. We can also create pipelines with packer post processors along with passing the varibles and outputs from one stage to another. 

Packer does not manage the snapshots or ami's created. They have to be manually clead up or removed after testing. 

# Docker 

We can do the same thing in docker as well including provisioning as well. This is much faster to the do in docker rather than aws. 